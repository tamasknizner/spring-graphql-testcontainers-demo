package com.example.springhelloworld.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.graphql.test.tester.HttpGraphQlTester;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.test.web.servlet.client.MockMvcWebTestClient;
import org.springframework.web.context.WebApplicationContext;

@Configuration
public class GraphQlTesterConfig {

    @Bean
    public HttpGraphQlTester tester(WebApplicationContext applicationContext) {
        WebTestClient client = MockMvcWebTestClient.bindToApplicationContext(applicationContext)
                .configureClient()
                .baseUrl("/graphql")
                .build();
        return HttpGraphQlTester.create(client);
    }
}
