package com.example.springhelloworld.user;

import com.example.springhelloworld.IntegrationTestBase;
import com.example.springhelloworld.data.UserEntity;
import org.junit.jupiter.api.Test;

import java.util.UUID;

public class UserControllerTest extends IntegrationTestBase {

    @Test
    void testGetUser() {
        UserEntity user = getUserRepository().save(new UserEntity(null, "John Doe", "john@doe.com"));

        getTester().documentName("userById")
                .variable("id", user.getId())
                .execute()
                .path("user.id").entity(UUID.class).isEqualTo(user.getId())
                .path("user.name").entity(String.class).isEqualTo(user.getName())
                .path("user.email").entity(String.class).isEqualTo(user.getEmail());
    }

    @Test
    void testGetAllUsers() {
        UserEntity john = getUserRepository().save(new UserEntity(null, "John Doe", "john@doe.com"));
        UserEntity jane = getUserRepository().save(new UserEntity(null, "Jane Doe", "jane@doe.com"));

        getTester().documentName("getAllUsers")
                .execute()
                .path("users.[0].id").entity(UUID.class).isEqualTo(john.getId())
                .path("users.[0].name").entity(String.class).isEqualTo(john.getName())
                .path("users.[0].email").entity(String.class).isEqualTo(john.getEmail())
                .path("users.[1].id").entity(UUID.class).isEqualTo(jane.getId())
                .path("users.[1].name").entity(String.class).isEqualTo(jane.getName())
                .path("users.[1].email").entity(String.class).isEqualTo(jane.getEmail());
    }
}
