create table demo_user
(
    id    uuid primary key,
    name  text not null,
    email text not null
);
