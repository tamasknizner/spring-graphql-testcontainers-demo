package com.example.springhelloworld.domain;

import com.example.springhelloworld.data.UserEntity;

import java.util.UUID;

public record UserRecord(UUID id, String name, String email) {
    public static UserRecord fromEntity(UserEntity entity) {
        return new UserRecord(entity.getId(), entity.getName(), entity.getEmail());
    }
}
