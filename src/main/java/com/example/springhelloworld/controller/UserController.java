package com.example.springhelloworld.controller;

import com.example.springhelloworld.data.UserRepository;
import com.example.springhelloworld.domain.UserRecord;
import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.QueryMapping;
import org.springframework.stereotype.Controller;

import java.util.List;
import java.util.UUID;

@Controller
public class UserController {

    private final UserRepository userRepository;

    UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @QueryMapping
    public UserRecord user(@Argument UUID id) {
        return userRepository.findById(id)
                .map(UserRecord::fromEntity)
                .orElseThrow();
    }

    @QueryMapping
    public List<UserRecord> users() {
        return userRepository.findAll()
                .stream()
                .map(UserRecord::fromEntity)
                .toList();
    }

}